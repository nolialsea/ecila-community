# User Guide

The "normal" use of the bots is normal conversation, you can talk to them as if they were real persons, 
and they will answer to their accords, sometimes sending multiple consecutive messages when they feel like it.

Each bot can be talked to in DM, as well as in their dedicated channel and optional multi-bot channel.
When talking to a bot in a multi-bot channel, their behavior will be slightly different, main difference being that they can ignore messages and decide not to speak.

## Emoji Buttons

The most common tools are designated as "Emoji Commands": there are emoji reactions that you can put on any bot message to perform various actions.

- Retry this message: 🔄 (`:arrows_counterclockwise:`) will completely regenerate the selected message and replace its content
- Continue this message: ▶️(`:arrow_forward:`) will generate a continuation of the selected message and edit the content to add the generated result to it
- Speak again, sending a new message: ⏩ (`:fast_forward:`) will make the bot send a new message
- Delete message: ❎ (`:negative_squared_cross_mark:`) will delete this message
  - **Caution: this command cannot delete your messages in DM, only the ones coming from the bot**

## Commands

Some commands don't have an associated emoji command and must be written as text.  
- Things in between (parenthesis) are optional.  
- Things in between \<brackets\> are mandatory.  
- You should never write those parenthesis and brackets.

### Reset
Command: `!reset (BotName)`
Effect: resets the **channel conversation history only** (it's just a marker in the conversation to tell the bot not to look atearlier messages)

The BotName part is optional and parenthesis shouldn't appear:
- When present, it will only reset the targeted bot (useful in multibot channels)
- When absent, it will reset whatever bot is present in the channel

Tip: resetting the conversation history is **reversible**, you can delete the message that says `!reset` to make the AI "remember" earlier messages

### Edit
Command: `!edit (MessageID) <New content>`  
Effect: edits either target or last bot message (only works on bot messages)

The MessageID is optional and parenthesis shouldn't appear:
- When present, it will target this message for edition
- When absent, it will target the last bot message for edition

Example: `!edit 56645649844545 Blah` will replace the text of the bot message with the ID `56645649844545` by `Blah` (right click on a message or long press on mobile to get the message ID **after enabling developer mode**)

### Impersonate

Command: `!impersonate <Message content>`  
Effect: impersonates a bot (make it say whatever you want)

`!impersonate Blah` will make the bot send a message containing `Blah`, then remove your message (doesn't remove your command message in DMs as it needs server permissions)

### Half Impersonation
Command: `!finish <Message start>`  
Effect: finishes a message (you write the beginning and let the AI complete the message)

`!finish Blah` will make the bot edit its last message and replace the content with `Blah` plus a completion of the message by the AI

`!finish 461521651484 Blah` will make the bot edit the message with id `461521651484` and replace the content with `Blah` plus a completion of the message by the AI

### User Alias
Command: `!alias <New Username>`  
Effect: changes your username in a channel (only from the bots' perspective)

`!alias Blah` will make all the bots inside the current channel see you as `Blah`

`!alias` will output your alias (in case you don't remember it)

- **Keep in mind that it only affects the current channel or DM channel**
- **Warning: it's not suitable to switch between multiple characters, there is another, better option for that**

### Sending messages from another character
Command: `<(as X)> <Message content>`  
Effect: impersonates someone else (this is suitable to switch between multiple characters)  
You can use this to integrate new characters into the conversation.  
**Important**: the parentheses here are mandatory!

`(as Blah) Hello!` will make the bot see the message `Hello!` with the author named `Blah` instead of your username/alias


**TIP: You can also combine it with the `!finish` commmand:**
- `!finish (as Blah) Hello,` will make the bot generate the end of the message `Hello,` from the perspective of the character `Blah`

# Next Chapter: [Bot Creation](https://gitlab.com/nolialsea/ecila-community/-/blob/main/BOT_CREATION.md)
