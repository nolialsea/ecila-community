# Legai, what it is?

It stands for "Lego AI", it's a standalone interface to create and test *AI workflows*  
A workflow consists of input/transformer/output bricks connected together to make data flow between them as they are processed  
Those workflows can then be imported and used in your code to easily integrate complex behaviors

![Full workflow example](https://i.imgur.com/POaAFem.png)

# How to use the interface?

When loading the page, you'll find yourself in front of a big white canvas: this is your **workspace**  
You can drop **generator files** from your computer to load them (sorry no other way right now, but it's planned)  
When a generator file is dropped, it will be loaded as a new **brick** with its corresponding inputs and outputs  

![Generator drag and drop](https://i.imgur.com/aFGe2QB.png)

As you can see, this brick has both inputs and outputs, which means it's a **Transformer** brick  
Transformer bricks by themselves won't be processed, they need to be connected to both **Input** and **Output** bricks, directly or indirectly  

## Output Bricks

Output bricks are the simplest: they just retrieve, display, and output data

![Output brick](https://i.imgur.com/DxuQUF3.png)

Any property that isn't linked will simply not appear in the result and be completely disregarded

![Output brick partly connected](https://i.imgur.com/IGQEEH5.png)

This behavior applies for all bricks, any property that is not connected will be disregarded as if it didn't exist throughout the whole graph  
This is especially useful when you want to reuse bricks in different ways  

## Input Bricks

Input bricks serve several purposes:
- They can generate completely new inputs for other bricks, as generators
- They can pass hardcoded values for testing (you enter each property value that should be passed to other bricks)
- They can pull their values from the code: it's the entry point of the workflow for devs

![Input Brick](https://i.imgur.com/ZW1JQh3.png)

Does this brick look familiar? It should, because it's actually the same Transformer brick imported above, except its type has been changed by selecting it and pressing the letter I  
You can easily change the type of any brick by pressing **I, O or T**, or by pressing escape and changing it inside the brick's settings (more on that later)

Same as for ouput bricks, any property not linked to another brick will be disregarded

## Transformer Bricks

![Transformer Brick](https://i.imgur.com/d9vDsyb.png)

The most interesting of the bricks... Transformer bricks use Generators to... transform data.  
Some examples of data transformation:
- Generate a character description from its SBF context
- Translate English to French
- Transform an input text into a more beautiful test
- Synthetise data from given text
- And way more

Note that transformer bricks properties can be either input, output, or **both**  
When a property is both input and output, it will simply use the input in the generator like any input, and pass the input value as output

## Connect Bricks Together

To connect blocks together, simply click on their output properties and connect them to an input property of another block (you can only connect forward right now)  
You can then also click the link and press the **Del** key to delete it

## Inside the Generators

When selecting a brick, it's possible to press Escape to edit its Generator file associated  
This will allow you to define everything the generator needs to do its job  
A Generator is nothing more than a fancy **few-shot prompt** builder and parser  

![Inside a Generator](https://i.imgur.com/Zuv5Pva.png)

This generator, once used, will produce a few-shot prompt that looks like that:  
```
[ Formatted Data Extractor: extracts formatted data from a given input. ]
⁂
Input: John loves his family
Output: [ Person: John; loves: his family ]
***
Input: The movie "The Avengers" was released yesterday and received great reviews!
Output: [ Movie: The Avengers; released: yesterday; reviewed: great ]
***
Input: There was a fight at the store, Bob got punched in the face by Henry!
Output: [ Event: fight; location: store; aggressor: Henry; victim: Bob ]
***
Input: CONNECTED_INPUT_VALUE_HERE
Output:
```

As you can see, the prompt is filled with incoming input data, and is left with the output to generate  
Once generated, this output will be automatically parsed back into a JSON object and mapped to the properties you've defined to be passed to next bricks


### Name

It's the name of the brick and the name of the generator. It will be displayed but not used by the AI or in code.

### Type

The type of the brick: input/transformer/output

### Description

Self explanatory

### Context

Now we're entering the good stuff... The context is a bloc of text that will be inserted at the very top of the generated prompt  
It's used to inject... context, into whatever generator you are doing. This is sometimes important to give instructions and rules to the AI

Generally, you'll want to use the Square Bracket Format (SBF) to describe concisely the context (see other docs for that)

### Properties

![Properties](https://i.imgur.com/7N6BBHj.png)

This field is made for you to define the input and output properties of the generator  
The properties should be a JSON **list** encasing one or several **dictionaries** (the property definition themselves)
Each property definition should have at least a:
- `"name"`: The name of the property and how it will appear in the GUI
- `"replaceBy"`: This value is used internally to build the few-shot prompt, it's used instead of the property name (**requires to put the colon character in most cases for best effect**)

In addition to those above, each property can also have:
- `"inputOnly"`: Will only show this property as input on the block
- `"outputOnly"`: Will only show this property as output on the block
- `"input"`: Completely bypasses the AI generation for that property and instead uses the provided string

![Property names](https://i.imgur.com/zjVnyzO.png)

Each property you define should appear in each example you provide in the `"list"` field next

Careful here, the order in which you define properties **and connect them** is very important:
- Properties are used in order to build the few-shot prompt
- Properties connected as input are always hoisted before others
- Properties connected as output are always inserted after others
- Orders between same type properties is kept as defined

### List (of examples)

![Examples List](https://i.imgur.com/ytEkrGW.png)

Now that the properties you want to use and generate are defined, it's time to give examples to the AI on how the properties correlate together!  
Same here, it's a JSON **list** encasing one or several **dictionaries** (the examples you give to the AI so it knows what to do)

Each item here should contain **all** the properties defined above, and provide a value for each of the property key `name`

Things to remember for examples:
- If you put it as an example, chances are that it will never appear during generation due to repetition penalty
- If you want to have broad generations:
  - either give very little examples (but extreme opposite ones) for more creativity, but results will be more random
  - or give **various and balanced** examples, as many as needed until the AI outputs only things you like
  - and tweak the **temperature** and **repetition_penalty** values in the **AI Parameters** (GPT settings, more below)
- If you want narrower generation
  - give many similar examples
- Test, test, test
  - every minor change you make on a generator can have drastic effects throughout the whole workflow
  - it's important to test several times to evaluate the tendencies in the output

### AI Parameters

They are purely technical GPT settings that you can edit individually for each generator, allowing precise control over the generation  
The two most important to know are:
- `temperature`: how random the outputs will be (1 is considered average, default 1.5, can go down to 0.1)
- `repetition_penalty`: how random the outputs will be (1 is like zero, never go below, default 1.046, don't go above 1.09 since it scales very quickly)

### Last Result

Just here to store the value to be passed to other blocks, and for display purposes

## Execute the workflow

Simply press the "**P**" key when you are on the main workspace, and if input and ouput bricks are connected, all the graph should be executed and the results of each blocks displayed! You can press "P" again once it's compeltely done generating (the green lines returns to grey)


# Simple Example from Scratch

If you've followed up until here, congratulation! I'm pretty sure you still have no clue how to put everything together, so let's see how to create a custom workflow from scratch!  
Let's say that we want to generate a greeting message when a new user joins our discord. To simulate this situation, we'll need 3 blocks:
- An input block: it will generate new user names in test environment (this app), and pull the real usernames in production (whatever code uses this workflow)
- A "transformer" block: it will take the username as input, and outputs a greeting message that takes this name into account
- An output block: it's what will be visible from the code after the execution of the workflow

First, take those three bricks, we will use them as blank bricks to start:  
[Blank Bricks](https://gitlab.com/nolialsea/ecila-community/-/blob/main/legai/blankGenerators.zip)

In the zip, you'll find 3 files:
- `I_BLANK_INPUT.generator`: the **I**nput block
- `T_BLANK.generator`: the **T**ransformer block
- `O_BLANK_OUTPUT.generator`: the **O**utput block

Let's drag and drop the input block file on the main workspace to load it:  
![Dropping input block](https://i.imgur.com/D1lgFiS.png)  

Once the block is loaded on the interface, you can select it by clicking on its title, it will then have blue borders:  
![Block with blue borders](https://i.imgur.com/HXXS6Ah.png)  

Now, press the `Escape` key to open the generator view, and you should see this:  
![Generator view on Input block](https://i.imgur.com/p6hvomz.png)  

The first thing to change is the **Context** value, this value will be inserted at the very top of the generated prompt as **context or instructions** for what the generator should do.  
![Context field to change](https://i.imgur.com/x6ecSwC.png)  
Let's change it for something more appropriate:  
```
[ Username generator: generates random discord usernames. ]
```  
This will allow the AI to know that we are trying to generate random usernames.


Like we said, our input should represent the usernames of our new discord users. To reflect that, let's change the **Properties** section a bit:  
```
[
  {
    "name": "username",
    "replaceBy": "Username:"
  }
]
```

Here, we define the property named `username`, and its "replaceBy" value to `Username:`  
The "replaceBy" value is what will be inserted in the few-shot prompt to keep track of properties and parse them back. It's important to put a **sensical** value here, something clearly qualifying what the property represents.

Finally, we want to add several examples in the **list** field to show the AI how the examples should look:  
```
[
  {
    "username": "DarkSasuke49"
  },
  {
    "username": "LordPasteque"
  },
  {
    "username": "IAmBread"
  }
]
```
**Note that the list contains several objects with the `username` property/value pair**  

We are done with this block, but don't forget to save it!  
![Save button circled](https://i.imgur.com/rnWhiol.png)  

Now that it's saved, you can close the generator view with the **Escape** key.  
This block should now know how to generate random discord usernames for our tests, so let's try that!

In order for a workflow to be complete though, it requires a minimum of two blocks: an input and an output block.
So... Let's just drop the blank output block (also, make sure to unselect the block by clicking anywhere, I think there is a bug here...):  
![Dropping the output block](https://i.imgur.com/i5j3KB7.png)  

Good, we have the two necessary blocks! Now let's just connect them by clicking on the **username** property of our input block, then the **output** property of our output block:  
![Connecting input](https://i.imgur.com/0eI6N0F.png)  
![Input successfully connected](https://i.imgur.com/RHZO7Pz.png)  

That's it. Now we can test the Input generator by pressing the **P** key!  
![I just can't xD](https://i.imgur.com/la0BC0q.png)  
Great, AI decided to give us fucking `BrockenButt` as random username... It means it works perfectly, yayy!  

Let's add our missing brick now: the transformer brick  
![Transformer brick full tuto](https://i.imgur.com/wqPe2PT.png)  

We'll need to reorganize some things, but for now let's select our new **Blank Transformer** brick by clicking its title and pressing **Escape**  
![Transformer brick full tuto generator](https://i.imgur.com/0QiIgKa.png)  

First thing to change, as always in a new generator: the **Context** field:  
```
[ Greetings generator: given an new discord member's username, generates a custom greeting message. ]
```

Here we described exactly what the generator is supposed to do to give as much context as possible to the AI so it can do its job correctly.

Let's change the **Properties** accordingly too:  
```
[
  {
    "name": "username",
    "replaceBy": "Username:",
    "inputOnly": true
  },
  {
    "name": "greetingMessage",
    "replaceBy": "Greeting message:",
    "outputOnly": true
  }
]
```

Here, we have one block input property and one output property  
If you remove the `inputOnly` or `outputOnly` fields, the properties will become **traversing** in the block:  
- traversing properties can be used either as block input or as generator
- if a traversing property has both input and ouput connected, it will transfer this value instead of generating one
- if a traversing property is only linked by its output, it will use the **list** examples to generate a new value

Anyway, I'll confuse you with more details later! For now, let's write some **list** examples for the AI to understand how the greetings should look like:  
```
[
  {
    "username": "DarkSasuke49",
    "greetingMessage": "Hello and welcome, DarkSasuke49! Please join us in the general channel so we can know you better!"
  },
  {
    "username": "LordPasteque",
    "greetingMessage": "Hey LordPasteque, welcome here! Feel free to join us in the general channel, everyone is there!"
  }
]
```

That should be enough examples so the AI gets the gist of it. Feel free to come back to the list examples and refine them as needed!  

Now let's talk about **AI Parameters**...  
As you see, this generator has two parameters defined:
- **`temperature`**: how random the result will be. Generally, you want to put the temperature value at 1 for normal answers, 0.5 for predictible answer, and 1.5 for less predictible answers. This value doesn't go below **0.1**, and going above 2 or 3 is not recommended.
- **`repetition_penalty`**: how much repetition should be avoided. Lowest possible value is **1.000**, and it's discouraged to go above **1.100**
  - This value is very sensitive, and my recommendations are: **1.046** for normal rates, **1.02** for generators that should output very similar results, and **1.06** if you want really varied results

For now, you can keep the current values, **Save** using the save button, then close the Generator view using **Escape**

Here I can see a bug =/ the link we previously put between our input and output block disappeared! Fear not, just moving the blocks will make it reappear.  

Once it's reappeared, select it by clicking on it, then press the **Delete** key to delete it.  

![Deleting connection](https://i.imgur.com/GOk8A6A.png)  
![Connection deleted](https://i.imgur.com/RvqjK4h.png)  

Let's move everything and connect the blocks in the proper order now:  
![Full workflow example 1](https://i.imgur.com/JuyxgIL.png)  

Then... Press **P**!

![Full workflow after execution](https://i.imgur.com/OTBQA5q.png)  

And... that's it. You now have a workflow that generates random users and greets them!  

Some things to remember:
- Don't put too many block output properties! A maximum of 150 tokens can be generated per block, so it's better to only have one output property, or several **short** ones
- The **list** examples are shuffled before the prompt is built, this is to increase the variety in results
- If a generator outputs nonsensical things: it may very well be because it's not explicit enough for the AI to understand what to do

## Some Exercises

### [EASY] Emotion detector
Given a message, detects the emotion of that message
- input brick: generates emotionated messages
- transformer brick: detects the emotion (neutral/happy/sad/angry/etc)
- output brick: outputs the detected emotion

### [MEDIUM] Generate a complex chatbot personality
Generate a chatbot personality: its username, full context and introduction  
Optional: generate even more information! Background, parents, life goals, create as much generators as you want to either extract or generate new informations for your characters!

### [HARD] Use the generated chatbot personality
Use the generated chatbot personality to greet new users and direct them to a random **existing** channel (forcing to select in a list rather than generating new random things)
- You'll need to create a **channel selector** generator that will pull from a defined list of channels
- You'll need to use that generator as input for the greeting generator so the AI knows to which channel to redirect the new user
- The greeting message should use the **chatbot personality** fed as input to create messages with the proper tone, according to said personality
