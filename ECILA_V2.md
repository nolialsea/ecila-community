# ECILA Bot Setup Guide

## Prerequisites
Before we get started, you'll need a few things:

- An ECILA API key from joining the Patreon at https://www.patreon.com/evilnoli - if you don't have one yet, just type `!key` in our discord server, or ask Noli!
- NodeJS v16.9 or newer installed on your system (get it from https://nodejs.org/en/download)
- (Optional but highly recommended) An OpenAI or Anthropic API key for access to cool features like auto memory, image generation, summaries etc.

***IMPORTANT NOTE!*** When using OpenAI or Anthropic, their fixed-price subscription ***DOES NOT INCLUDE API USAGE***. In other words, you can't just use the fixed price subscription with the ECILA chatbots, you need to use their API, which are paid-per-use!

## Creating Your Discord Bot

1. Head over to https://discord.com/developers/applications and create a new application.

2. In your new application, navigate to the "Bot" section from the left menu and create a bot.

3. The "Reset Token" button lets you generate or reset the bot's token - this token is required for configuring bots, so don't lose it! Resetting will break existing bots using the old token.

4. Make sure the "Public Bot" option is turned off to avoid listing your bot publicly. 

5. Enable the "Presence Intent", "Server Members Intent" and "Message Content Intent" options to ensure all features work smoothly.

6. To add your bot to a server, you'll need to generate an invite URL. Enable the "Administrator" permission for now to avoid permission issues with future updates.

7. Alternatively, you can select just the minimal required permissions if you prefer, but some features may be unavailable.

8. Copy the generated invite URL, open it in your browser and select the server you want to add the bot to.

## Getting an OpenAI API Key (Optional)

1. Visit https://openai.com and sign in/create an account.

2. Click your profile picture in the top right and select "View API keys".

3. From there, you can create a new secret API key to use OpenAI's models.

## Installing and Configuring the App

1. Get the latest ECILA app v2 code from the GitLab repo at https://gitlab.com/nolialsea/ecila or download the ZIP.

2. Extract the files to a folder on your machine.

3. Open the `config.json` file in the root of the project - this is the main configuration file and most settings are explained there.

4. Enter your ECILA and OpenAI API keys (if you have one), and adjust any other settings as needed.

## Understanding the Configuration Files

The main `config.json` file, as well as any other config files loaded by the app, contain vital information in the form of comments. These comments, denoted by lines starting with a `#`, provide explanations and documentation for each setting.

It is **extremely important** to carefully read through all of the comments in these config files. They explain the purpose of each setting, provide examples, and offer guidance on configuring the app properly.

**Do not simply skim over or ignore the comments!** Taking the time to understand the configuration options will ensure you set up the app correctly and avoid issues down the line.

Some key things the config comments cover include:

- API keys and authentication
- Role and permission management  
- Customizing the bot's behavior
- Connecting to external services like image generation

Pay close attention to any lines starting with `#` in the config files. Treat the comments as essential documentation to get the most out of the ECILA app.

## Running the App

1. Open a console/terminal window in the project folder.

2. Run `npm install` to install dependencies if you haven't already.

3. Start the app with `npm start`.

The app will now build, boot up, load your config and start any configured bots!