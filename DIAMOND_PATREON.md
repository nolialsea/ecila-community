# You are a Diamond Patreon, yay! Now what?

Being a Diamond Patreon gives you the access to the executable ECILA application, thus allowing you to host your own bots on whichever discord server you have permission to invite bot into!  

# Prerequisites for NovelAI API (latest and current)
1. A paid NovelAI account (any tier should do, but it's preferable to use Opus)
2. Grab your NovelAI API key (see https://discord.com/channels/851625855062638603/851625855528075317/883549059271491624)
3. Install NodeJS version 16.x or more [HERE](https://nodejs.org/en/download/)

# Grab the latest release
You can get the latest build release on discord in the `#diamond-patreons` channel in the official ECILA discord server (you need to have the `Diamond Patreon` role).  
The latest release should be pinned here! Just download it and extract it anywhere.

# Install
Open a console and navigate to the application folder, then:

1. `npm install` to install node modules
2. `node configure.js` to configure the app and create the necessary files

For Windows: you can open a console by simply shift+right clicking on the folder and select "Open Powershell window here"  
![Open console](https://i.imgur.com/uT953Yl.png)

# First launch

1. You'll need to create one or more discord bots through discord's website, check here for a tutorial: https://imgur.com/a/reC2kyC
2. In `node configure.js`, you'll be asked questions in the console to setup the configs and your first bot
3. This bot is your `Master Bot`, it's important to have one and only one per discord server with bots you host
    - This master bot will handle the global actions performed on the servers, if there are multiple master bots, they will all do the same actions at the same time, including deleting the same message multiple times (which is bad)

# Start/Stop the bots
Open a console and navigate to the code folder, then:
1. `node index.js` to run the application
2. `ctrl + C key` on the console to stop the application


# Configure your discord server
You'll have to create a discord role named `Bot Administrator` to give to yourself and all the persons you want to give edition access to your bots.  
Role itself doesn't need any permission, it just acts as a flag to tell the bots who is admin of them or not.

# How to update the code
Simply grab the latest release of the code in the discord channel #diamond-patreon, open the zip file, select all the files, and drop them on your previous installation by overriding the files that changed.  
All your previous configurations should work fine, unless specified otherwise in the release. I try to keep things as retroactive as possible, and will write migration tutos when not possible.

# How to update the config file
New updates are likely to add new config properties, but current code won't add those to your config files by default.  
To update your config file and add the missing properties, simply use `node updateConfig.js`

# The `config.json` file
It's the main configuration file for everything global for the app. The properties that are not mentioned below are the ones you should probably not touch after first launch. If a property below doesn't exist in your file, you can add it manually, otherwise specified default values will be used.  

```
{
    ...

    "maxMessageMemory": 100,

    // Change this to 1024 if you have a Tablet NovelAI sub tier. Defaults to 2048
    "contextSize": 2048,

    // ID of the bot that will greet newcomers. Defaults to nothing
    "greetingBotId": "2615484155484544",

    // Channel name of the greeting channel, without the hashtag, all lowercase and with dashes instead of space (discord channel name format). Defaults to nothing
    "greetingChannel": "newcomers",

    // Text file that will be used to generate the greetings. File needs to be a ".txt" in the "customPrompts" folder
    "greetingFile": "greeting",

    // JSON file in "presets" folder that will be used for the text AI generation settings (without ".json"). Defaults to "krake.json" if you have an Opus sub tier, "euterpe.json" otherwise. If you want to edit the preset, I highly recommend to change this value instead of editing the default files, as those will be overriden on each code update
    "presetFile": "krake",

    // Only enable if you have an Opus sub tier. Will use anlases if you don't have Opus. Defaults to false
    "enableImageGen": false,

    // Disable TTS if you encounter rate limit problems (general bot lag). TTS takes a lot of AI requests compared to text conversation
    "disableTTS": false,

    // Timeout in minutes for the !create command and other potential commands with a timeout
    "generatorTimeoutInMinutes": 1,

    // If your discord server has a main channel where you'd like some of your bots to be in, put the channel name here. Channel will be considered as a special multibot channel
    "mainChannel": "general"
}
```

# Experimental features
To enable features listed below, you'll have to enable them in the `config.json` file using this line:
```
"enableExperimentalFeatures": true
```

## Random Smart Answer
This feature allows the bots in **every multibot channels** to randomly perform a smart answer check
```
"enableRandomMessage": true,
"randomMessageChanceInMinutes": 60
```
In this example, each second, each bot present in a multibot channel will have a slight chance to perform a smart answer check (`1/(60 * 60)` chance every second).  

## Turbo Smart Answer
This feature allows the bots in **one multibot channels** to randomly perform a smart answer check using a different timer value
```
"enableTurboMode": true,
"turboModeTimerInSeconds": 60
```
In this example, each second, each bot present in the turboed multibot channel will have a slight chance to perform a smart answer check (`1/60` chance every second).  
