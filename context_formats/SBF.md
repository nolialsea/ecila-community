# What is SBF?
SBF for **Square Bracket Format** is one of the emergent contextual data formats that the GPT family of AI models understands the best, while still keeping a low token count.  
It has its origins from the metadata in the training set for training and finetune.

**Pros**:
- Easy to learn
- Very compact: takes less tokens than W++

**Cons**:
- Less powerful than W++, AI will "remember" things a bit less reliably (still very effective)

It looks like this:  

```
[ Class: Object Name; attribute: attribute value; listAttribute: value 1, value 2; proseAttribute: long string ending with a dot. ]
[ Character: Alice; gender: female; appearance: cute, blonde, blue eyes; personality: joyful, friendly, curious; description: Alice is your typical cute girl. ]
```


## Explanation
- The `Class` is what your SB block represents, it always have a first letter uppercase because of conventions, and also because tokens with an uppercase have a slightly different meaning that fits best for class identifiers (probably in fact because of code conventions)
- The `attribute` is fully lowercased to fit to a "mid-sentence" token, so it indicates to the AI that this word is comprised into the Class stuff as "child" data
- The `listAttribute` doesn't have a space between words and instead use an uppercase, this is a code convention too but in the case of W++ it's also to avoid the AI being confused and think the first word is an attribute with missing parentheses
    - The two values `value 1, value 2` of the `listAttribute` could also be represented as `value 1 + value 2` (closer to code convention), but in cases of listed values like that the AI has no problem using the same "string" and still accounting for comas to be a separation between internal values of the string, and it uses less tokens
- If you need to write full sentences in values like in `proseAttribute`, it's generally better to put them at the end of the SBF block not to pollute the more typed data
    - **never alternate between prose and short values, keep prose at the end, put a dot at the end of the sentence and capitalize first letters after dots if there are multiple sentences**
- Note that there are no double quotes like in W++: the semicolon token is explicit enough to represent a separation between attributes

## When to use leading spaces and capitalization

Words aren't tokenized into the same tokens depending of if there is a trailing space or their casing:  
`test`  
`Test`  
` test`  
` Test`  
All of those are different tokens, different "words", to the AI. They all mean somewhat the same thing, but not exactly, and might not share the same connection weights to other tokens

It's generally better to use the tokens that are the most represented in the dataset since they are most likely to have the highest connection weights to other stuff: lowercased words with a space before them.  
But in some cases, you might prefer to use the uppercased version or the version without trailing space, just because they have weights that fit better to your needs (class identifyer for example)  
An important exception here are names, that should always be properly capitalized
