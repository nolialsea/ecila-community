# What is W++?
W++ is one of the emergent contextual data formats that the GPT family of AI models understands the best, while still keeping a low token count.  
It is emergent from C++ and code standards in general.

**Pros**:
- Powerful, helps the AI "remember" things more consistently

**Cons**:
- Takes a bit more tokens than SBF

It looks like this:  
```
[ Class("Object Name") { attribute("attribute value") listAttribute("value 1, value 2") proseAttribute("Long string ending with a dot.") } ]
[ Character("Alice") { gender("female") appearance("cute, blonde, blue eyes") personality("joyful, friendly, curious") description("Alice is your typical cute girl.") } ]
```


## Explanation
Since it's inspired from C++:
- The `Class` is what your SB block represents, it always have a first letter uppercase because of conventions, and also because tokens with an uppercase have a slightly different meaning that fits best for class identifiers (probably in fact because of code conventions)
- The `attribute` is fully lowercased to fit to a "mid-sentence" token, so it indicates to the AI that this word is comprised into the Class stuff as "child" data
- The `listAttribute` doesn't have a space between words and instead use an uppercase, this is a code convention too but in the case of W++ it's also to avoid the AI being confused and think the first word is an attribute with missing parentheses
    - The two values `"value 1, value 2"` of the `listAttribute` could also be represented as `"value 1" + "value 2"` (closer to code convention), but in cases of listed values like that the AI has no problem using the same "string" and still accounting for comas to be a separation between internal values of the string, and it uses less tokens
- Double quotes help to delimit the values and the **type** of the values, which can (and maybe **shall**) be used wrong: GPT prefers strings over numbers, that's a fact, and "forcing" it to interpret most things as strings can prevent the cases where the AI would try to interpret them as numbers or other things

## When to use leading spaces and capitalization

Words aren't tokenized into the same tokens depending of if there is a trailing space or their casing:  
`test`  
`Test`  
` test`  
` Test`  
All of those are different tokens, different "words", to the AI. They all mean somewhat the same thing, but not exactly, and might not share the same connection weights to other tokens

It's generally better to use the tokens that are the most represented in the dataset since they are most likely to have the highest connection weights to other stuff: lowercased words with a space before them.  
But in some cases, you might prefer to use the uppercased version or the version without trailing space, just because they have weights that fit better to your needs (class identifyer for example)  
An important exception here are names, that should always be properly capitalized
