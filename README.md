# ECILA

Welcome to ECILA

# Table of Contents
1. [Introduction](#introduction)
2. [This Repository](#this-repository)
3. [Official Discord Server](#official-discord-server)
4. [Channel Types](#channel-types)
5. [Bot Behavior](#bot-behavior)
6. [Tips and Tricks](#tips-and-tricks)


## Introduction

ECILA is a discord chatbot application that allows to create, edit and host one or many discord AI bots.  
It mainly uses NovelAI as the backend for AI generation, with possible KoboldAI alternative backend support (experimental, only colab TPU support right now).  

ECILA is the fourth complete rewriting of ALICE (previous version available [here](https://gitlab.com/nolialsea/alicebot)), the original chatbot project, that wasn't more than a simple AI Dungeon script back in September 2020.  
The project itself keeps evolving and adapting to current events since then, changing platforms and backends as new and better stuff appears.

## This Repository

Here will be hosted the documentation of this project, as well as various tips and tricks or researches from the community.  
Contributions through Merge Requests or Issues is allowed, even encouraged for everyone!  
As for the code itself, it's currently hosted on a private repository, and is not open to public right now.  
The application, however, can be used to host the bots yourself, but is only available to my highest patreon tier (might change in the future).

## Official discord server

There is an official discord server currently named "ECILA" (previously AliceBot) that you can join [right here](https://discord.gg/W5TVnzVkSX).  
A lot of AI chatbots are hosted here by myself, some are official and maintained by me, others are community created, either on "Open Bots" or "Patreon Bots", bots provided as a reward when you support me!  
Feel free to pop in and meet the community and the bots, there is always ~~weird~~ interesting stuff happening there!

# Privacy
Probably your main concern: no, the code doesn't store nor log your private conversations with the bots!  
Privacy is primordial when it comes to having a sincere conversation, so no private message content will ever appear anywhere on the server side.  
That said and to be completely transparent: **activity** itself is logged for metrics purpose, meaning that the server keeps track of when a bot talks or has been talked to (your username won't appear tho, you're still incognito)

## Where to start?
Probably the easiest way to start when you join the discord server is to simply DM a bot.  

## Private Messages
To send private messages to a bot, simply check the bot list at the right corner, right click any of them, then press "Message".  
![How to DM a bot](https://i.imgur.com/KFaOyLy.png)  
Every bot has a different personality, so try several of them!  

The conversation happens in the natural way, you tell the bot something, and it will answer after few seconds.

## Public channels
Before you begin, it's important to know that not every bot looks at every channel in the server.  

### Dedicated Channels
Most if not all bots will have one and only one **dedicated** channel where only one bot is present, it's the channel where their personality can be edited by their owners.  
In a **dedicated** channel, the bot will always answer you after some seconds. This delay isn't a lag, its main purpose is to leave you some time to send multiple messages if you want. More on that later.
Those dedicated channels are all included in these channel categories depending on owner type:  
![Channel categories: official bots, patreon chatbots, open access bots](https://i.imgur.com/tZwc6bY.png)

### Multibot Channels
In addition to this dedicated channel, a bot can also be present in one or more **multibot channels**.  
Those channels are generally themed, using the channel topic as global context shared among all bots present inside it.  
Bot owners can move their bots from one multibot channel to another, and admins can also add them onto multiple ones.  
You'll find all the open access multibot channels in the `MULTIBOT CHANNELS` category, but admins can also add a bot onto any channel if they want.  
In multibot channels, **bots won't necessarily reply to messages** unless some conditions are met to trigger them. See the "Smart Answer" section (to be written)  
The #general channel is a bit special, as it's considered multibot but mostly contains official bots. Only admins can add bot there.  

### Private Messages
Last and most importantly: **all the bots can be talked to in private through DMs**, just right click their avatar and press "Message" to start a conversation!

### Recap
Bots can be talked to in three types of channels:  
- Direct Messages: you can DM any bot and converse with it as you want, with almost all commands enabled
- Dedicated Channel: each bot has one dedicated public channel where it will always answer. Commands here are restricted to bot owner and admins
- Multibot Channel: some channels allow multiple bots to be inside and talk to each other. This mode is a bit different than the other ones, and the bots will not behave in the same way (more in the Smart Answer section) 

## Bot Behavior

Depending on the channel you talk to the bots in, their answer conditions will be handled differently.
There are two kind of behaviors for the bots: mono-bot channels and multi-bot channels.

### Mono bot channel behavior

Mono is the default, when you talk to a bot in its **dedicated** public channel or in **DM**.  
In this mode, the bot will always send a message back after some delay (14-18sec) when you talk to it.  
Tip: You can use that delay to send multiple consecutive messages to the bots

### Multibot channel behavior

Multibot is for channels where there are multiple bots (`#general` and `#multibot-xxx`)  
In those channels, the bots won't answer to each message like in mono-bot channels.  
Instead, each time they should normally answer, they instead perform a check to determine if they want to answer or not.  
Note: there are some false positive and true negative in this behavior, sometimes the bot will think you want to send a consecutive message instead of directly replying.  
Tip: Use the :fast_forward: (`:fast_forward:`) reaction emoji to make a bot talk!  
Tip: Mentioning the exact name of a bot or replying to one of its messages will always trigger an instant response from it!  

## Tips And Tricks
- If the AI doesn't make any sense, retry or edit its answer!
  - Coherence is extremely important, and incoherencies are likely to snowball out of control
  - The tidier you keep the conversation, the better the results will be
- **Don't repeat yourself**
  - Repetition is bad for the AIs
  - If you repeat stuff, they'll do too
- **Use third person text in between asterisks or backquotes**
  - Text you enter in between asterisks or backquotes will be seen by the AI as **third person text describing actions and body gestures**
  - If you use first or second person perspective, it will break coherence and make the AI dumber
- You can use **context injection** by **starting** and **ending** your message with square brackets
  - Contextual messages will be injected with square brackets and **without author** in the conversation (the AI won't know that it's you that said it, it will just see it as an author's note)

# Next Chapter: [The User Guide](https://gitlab.com/nolialsea/ecila-community/-/blob/main/USER_GUIDE.md)
