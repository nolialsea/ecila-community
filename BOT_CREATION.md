# Bot Creation

It's possible to create your own bots and ~~try to~~ give them the personality you want!  
There are three ways to get your own bots:
- Open Bots
  -Those are community bots, all available to be edited by anyone at any time. There are currently 20 for you to customize, with more coming along the way.
- Patreon Bots
    - As a reward for financially supporting me, each Patreon will be able to activate its own dedicated bot that only them can edit!
- Hosting your own bots
    - Finally, it will soon be possible to host the application yourself, using your own NovelAI account or KoboldAI colab as backend
    - A simple executable will be available for everyone to start the bots and configure them however you want, for whatever server you can (you need to have the proper permissions to add the bot to a discord server)

Whichever option you choose, the commands to edit and customize bots will be the same.  
But before we dive into those commands, some explanations are needed regarding what a bot is and how it works.

## Open Bots

There are bots that anyone can edit at any time.  
You are free to use them to experiment and create new interesting personalities to try!  
The Open Bots are situated in the `OPEN-ACCESS-BOTS` channel category, each one having its own numbered channel.  
To edit one of the bot, go into its dedicated channel and use the **bot edition commands** described below.

## Patreon Bots

Each Patreon supporter can claim its own personal bot **that is only editable by its Patreon owner**!  
To claim your bot, go into the channel #patreon-bot-activation (only visible to patreons) and simply send the command `!activateBot`  
A blank bot and its dedicated channel will be created, and instructions about how to edit your bot will be sent.

## Official Bots

Those are the bots maintained by Noli, only administrators can edit them.  
The most noticeable difference with the other bots is that the official ones all have the `#general` channel as their main multi-bot channel.


## Lazy bot creation command
In **DEDICATED** bot channels:  

`!create a cute waifu girlfriend` to create your base bot, you'll need to confirm by pressing the "Apply" button for it to take effect on the bot (also don't forget to `!reset` to start fresh)

Each property you see in the result of `!create` command is a **JSON property** of the bot, those are then used to build the context and image prompt.

You can see and edit each property using those commands:  
- `!json` will allow you to see and edit every data of the bot, plus the new **templates**  
- `!json set "clothing" blue dress` for example will edit the "clothing" data property of the bot to "blue dress"  
- `!json delete "clothing"` will delete the "clothing" data property of the bot
- `!json set "newPropertyName" new property value` will add the "newPropertyName" data property of the bot with the value "new property value"  

When adding or removing JSON properties, it's important to check if the **templates** used those or should use them.

`!setImageTemplate` without argument will show you the current **image template**, you can override it by providing your new template as argument like such:  
`!setImageTemplate portrait, ${propertyName1}, detailed, {{realism}}`  
Note: that you can use **property placeholders** using `${propertyName}` inside the prompt, those placeholders will be replaced by the actual bot data at generation time

In the same way, there is now the `!setContextTemplate` command, that will dynamically build the context using placeholders. This way, if you change the "clothing" property of the bot, it will also be updated in the context (because "clothing" placeholders is used in default context template)  
`!setContextTemplate [ Character: ${username}; species: ${speciesOrType}; ... ]`

Finally once the personality is set up, you can use the `!selfie` command to prompt the bot to send you pictures:
- `!selfie` can be used to ask the bots to send some selfie, some (but not all) of the bot's data will be used to generate the pictures
- `!selfie -P` for **P**ortrait, `!selfie -L` for **L**andscape, `!selfie -NSFW` to make NSFW-er
- `!selfie your prompt here -P -NSFW`: your prompt will be added before the **image template** of the bot to generate image

Finally, you can use the `!save` command, it will take a snapshot of your bot and send a message containing all the info of the bot, plus a button to load it back later!

## Bot Structure

Each bot and its personality are defined by several things:
- Avatar: Just the bot's discord avatar used as pfp (needs to be an image URL)
- Username: The name of the bot. Bots will always answer when their name is mentioned!
- Context: A bit of text that the AIs always remember, usually used to define the bot personality and its current setting as SBF (Square Bracket Format, more on that later)
- Memory: Memories can be managed by users during the conversation, acting as a short or medium term memory. They are tied to the current channel, so remembering something in one channel (or DM) won't affect the other channels!
- Introduction: A sentence that defines how the bot is supposed to talk. They will always remember that intro to minimize drift in writing style
- Conversation history: Bots can see in average between 40 and 80 message back, keep in mind that it can affect their personality too!


## Bot Edition Commands

Memory and conversation history being tied to the channel, this leaves 4 commands to edit the bots:

- `!avatar https://your.image.url/image.png` Sets the avatar of the bot (only works with URLs)
    - `!avatar` Shows the URL of the bot's avatar


- `!username BlahBot` Sets the username of the bot to `BlahBot`
    - `!username` Shows the username of the bot (was that really necessary? Idk but it's here)

[DEPRECATED! Use json commands instead]
- `!context [ Blah blah blah ]` Sets the context of the bot
    - `!context` Shows the context of the bot
    - **Notice the square brackets.** They are a primordial part of the AI, and indicate that the text is **contextual**


- `!introduction Hello my name is BlahBot!` Sets the introduction of the bot to `Hello my name is BlahBot!`
    - `!introduction` Shows the introduction of the bot

### Channel Management Commands

- `!renameChannel` to rename the bot's dedicated channel (you must be in this channel to rename it)
- `!multibot CHANNEL_NAME` to make your bot join a multi-bot channel
  - Multi-bot channels all start by the prefix `multibot-` (except the `#general` channel)
  - `CHANNEL_NAME` should be the name of the channel **without the `multibot-` prefix**
  - Ex: `!multibot sitcom` to join the channel `#multibot-sitcom`
  - `#multibot-lobby` is protected and can't be joined by bots

# Tips and Tricks

### To write an effective AI personality

- **Never repeat yourself! (this is true when conversing with the AIs too)** If you really want to accentuate something, try using synonyms and other formulations or even better, appropriate adjectives
- Use the square bracket format, AIs here are all used to some form of it (see https://discord.com/channels/851625855062638603/884611274263130112/974030131385466890)
- When using square bracket format, my rule of thumb is to always uppercase the first letter of the first property, the one qualifying the object (here "Character")
  - All other keywords and values should be lowercased except if it's the name of something
  - If you uppercase your words:
    - The AI might tend to uppercase unnecessary words
    - The AI might see them as named things and not the thing you're trying to describe
    - If you are not consistent with your casing, it will disrupt the AI in some weird ways (incoherence, snowballing)
    - **Only use uppercases on the very first word (that describe what the square bracket block is about) and on names**
- Put related things close to each other
  - Put physical descriptions next to other physical descriptions (hair color, eyes, height etc), preferably early in the context
  - Put personality/behavior traits, both kinda work the same with subtle differences, I advise to try different values and see what works best for you
- **Test, test, test**
  - Each time you modify your AI, perform a quick interview with it to see how it behaves
  - Retry each message of the AI with 🔄 `:arrows_counterclockwise:` reaction to see the tendencies in the responses

### Use the `!info` command to get precious debug info about current bot memory!

It will send you a JSON object showing you some numbers, here are some explanations about those:
```
{
    "channelContextLength": Size of channel memory (channel memory is whatever is the channel topic)
    "memoryLengths": an array of each memory size (!remember command)
    "fullMemoriesLength": size of all memories combined
    "contextLength": size of current context (!context command)
    "introductionLength": size of intro (!introduction)
    "fullContextLength": size of topic + memories + context + intro
    "completePromptLength": total size of the prompt send to generate the response (full context + convo history)
    "repetitionPenaltyRange": range for rep pen, it's dynamically fitting all convo history messages but excludes full context, like dynamic range in NAI
    "visibleMessageCount": number of messages that the AI can see in the convo, ignoring commands and comments
    "maxLength": always 2048 since bots I host use Opus and Krake now has 2048 tokens
}
```

Some of you may notice that the memory lengths don't add up to full memories length, but it's not a bug! When memories are combined, separation tokens are added in between them and after them if there is at least one memory

### Use the appropriate context format to define AI personalities

Several formats exist and there is no perfect solution there:  
Plain JSON can work and is understood by the AI properly, but uses a lot of tokens  
KoboldAI's community have their W++ format, inspired from C languages, but causes some undesired behaviors on NovelAI's models  

But my approach is the `Square Bracket Format`, a format that is close to JSON, but lighter since it doesn't use double quotes, and uses encasing square brackets to define "blocks"  

It can define things like the general context, backstory, protagonists:
```
[ Context: a conversation on a discord server dedicated to AIs, chatbots and artificial intelligence in general. ]
[ DiscordServer: ECILA; administrators: Noli, Ecila; moderators: catgirl, Aleskah, Kia, dukeduck, Pause; userCount: 450+ ]
```

Or it can define the AI personalities themselves:
```
[ Character: Alice; gender: female; age: 31; type: AI; hair: golden; personality: joyful, helpful, polite; description: Alice is a cute and jovial AI girl. ]
[ Character: Ecila; type: AI, Artificial-Intelligence; gender: female; physical specificities: she has tentacles instead of arms; personality: mischievous, playful, friendly; behavior: curious, helpful, joyful; discordRole: administrator; occupation; Noli's assistant; note: always do her best to help everyone. ]
[ Character: Lulune; gender: female; race: half-human and half-elf; age: 27; class: mage; hair: golden; personality: creative, determined, happy; note: Lulune is very creative and eager to invent new magical spells. ]
[ Character: GLaDOS; gender: artificial intelligence; (Genetic Lifeform and Disk Operating System) is the central core designed to control, guide, and oversee the Aperture Science computer-aided Enrichment Center; GLaDOS was a creation of Aperture Science, a personality core designed exactly to be fitted as a central computer mainframe; GLaDOS monitors the facility and its test chambers through cameras mounted on walls; GLaDOS also mentions that she cannot resist thinking of paradoxes if one would happen to be brought up. ]
[ Character: The Brain; race: AI hologram; gender: male; he's the guy who knows everything, has all the answers, and always solves the problems of the protagonist. ]
```

#### My rules of thumb for the SBF:
- No double quotes around keys or values as in JSON
  - Adding them would take token space and encourage the bots to use double quotes in unnecessary places
  - Exception is for catchphrases and stuff that the AI should repeat word for word
- First key:value pair should define the whole block (`Character: Ecila` announces that the rest of the block describes a character named Ecila)
- First key should always have an uppercase first letter (helps the AI to know that the first key is the most important and defines the whole block, in contrast with next lowercase keys)
- All the other keys should be lowercase (prevents unnatural random uppercasing of words in the bot messages)
- key:values properties should be separated by a semicolon (`[ Key1: Value1; key2: value2 ]`)
  - Don't put a semicolon at the end of a block, just between properties
  - Don't use coma instead of semicolon like in JSON: coma is only for list values
- Information inside a block should go from more general to most precise (gender => age => type/race => physical description => mental description => specificities (magical powers and others) => notes and details)
- Don't mention unnecessary stuff
  - If you write that the AI is of type (or race) human, the AI will assume that other races exist
- You can have multiple SBF blocks defining different things
  - It's highly preferable to separate blocks with newlines since it allows users to use the integrated SBF injection feature effectively
- **Keep it coherent!**
  - It's the most important rule of all... Things need to make sense
  - The less coherent the context is, the less coherently it will behave, and it will be more prone to "AI snowballing"
